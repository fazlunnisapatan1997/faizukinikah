package com.te.techno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechnoElevateApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechnoElevateApplication.class, args);
	}

}
